import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:phonemia/screens/home_screen.dart';
import 'package:phonemia/screens/uploader.dart';

import 'screens/login_screen.dart';

// These are some changes
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: HomeScreen.routeName,
      routes: {
        HomeScreen.routeName: (ctx) => HomeScreen(),
        LogInScreen.routeName: (ctx) => LogInScreen(),
        UploaderScreen.routeName: (ctx) => UploaderScreen(),
      },
    );
  }
}
